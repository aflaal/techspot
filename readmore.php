<?php 
session_start();
/* REUSABLE FUNCTION */
require 'models.php'; 
    /* FETCH SINGLE ROW */
    $blog = getCertainBlogRow();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TECHSPOT | <?php echo $blog->title; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>

      <!-- NAV BAR -->
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark sticky-top">
       <div class="container">
           <?php if(isset($_SESSION['username'])){echo '<a class="navbar-brand h4" href="admin.php">TECH<span class="text-success">SPOT</span></a>';}
           else{echo '<a class="navbar-brand h4" href="index.php">TECH<span class="text-success">SPOT</span></a>';}
           ?> 
           <ul class="navbar-nav">
             <li class="nav-item active"><a class="nav-link" href="admin.php"> <b> Home</b></a></li>
             <li class="nav-item"><a class="nav-link" href="insert.php"><b>ADD NEW POST</b></a></li>
             <form class="form-inline my-2 my-lg-0 mr-3">
               <input class="form-control mr-sm-2" placeholder="Search" name="search">
               <button class="btn btn-outline-success my-2 my-sm-0"  type="submit">Search</button>
             </form>
             <a href="login.php" class="btn btn-outline-warning btn-md mr-3" role="button">LOGIN</a>
             <?php if(isset($_SESSION['username'])){ echo '<a href="logout.php" class="btn btn-outline-danger btn-md" role="button" >LOGOUT</a>'; } ?>
           </ul>    
       </div>
      </nav>

      <!-- JUMBOTRON -->
      <div class="jumbotron jumbotron-fluid bg-primary text-light">
        <div class="container">
          <hr class="md-4 bg-light">
          <h1 class="display-3 text-center"><?php echo $blog->title; ?></h1>
          <hr class="mt-4 bg-light">
        </div>
      </div>

      <!-- Readmore Post goes here -->
      <div class='container mb-5'>
        <p class='lead text-justify'><?php echo $blog->fullbody; ?></p>
        <a href="index.php" class="btn btn-outline-success btn-md" role="button"> ← BACK TO THE HOME</a>
      </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>
