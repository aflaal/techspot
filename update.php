<?php 
session_start();
/* REUSABLE FUNCTION */
require 'models.php'; 
    /* GET CERTAIN DATA FROM BLOG TABLE */
    $blog = getCertainBlogRow();
    /* UPDATE DATA FROM BLOG TABLE */
    if(updatePost()){
      $message = "Post updated Successfully";
    } 
?>
 
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TECHSPOT | UPDATE POST</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>
     
     <!-- NAV BAR -->
     <?php include 'nav.php' ?> 

      <!-- JUMBOTRON -->
      <div class="jumbotron jumbotron-fluid bg-success text-light">
        <div class="container">
          <hr class="md-4 bg-light">
          <h1 class="display-3 text-center">UPDATE PANEL</h1>
          <hr class="mt-4 bg-light">
        </div>
      </div>

      <!-- SUCCESS MESSAGE -->
      <?php if(!empty($message)): ?>
      <div class="alert alert-success container" role="alert">
          <?php echo $message; ?>
      </div>
      <?php endif; ?>
      
      <!-- FORM -->
      <form class="" action="" method="post">
        <div class="container">
            <div class="form-group">
            <label><h4>Post Title:</h4></label>
            <input value=" <?php echo $blog->title; ?> " type="text" name="title" class="form-control form-control-lg" placeholder="Enter the post Title">
        </div>

        <div class="form-group">
            <label><h4>Post Contents:</h4></label>
            <textarea rows="6" name="body" class="form-control form-control-lg" placeholder="Enter Your Post.."><?php echo $blog->body; ?></textarea>
        </div>

        <button class="btn btn-danger mb-5 btn-block" type="submit"><b>UPDATE POST</b></button>
        </div>
      </form>
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
   </body>
  </html>
