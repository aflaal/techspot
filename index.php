<?php 
/* REUSABLE FUNCTION */
require 'models.php';
  /* GET ALL DATA FROM BLOG TABLE */
  $blog = getAllBlogRow();
  /* SEARCH BUTTON FEATURES */
  if(isset($_GET['search'])){
    $blog = searchPost();
  }


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TECHSPOT | HOME</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>

      <!-- NAVBAR -->
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark sticky-top">
       <div class="container">
           <a class="navbar-brand h4" href="index.php">TECH<span class="text-success">SPOT</span> </a>
           <ul class="navbar-nav">
             <li class="nav-item active"><a class="nav-link" href="index.php"> <b> Home</b></a></li>
             <form class="form-inline my-2 my-lg-0 mr-3">
               <input class="form-control mr-sm-2" placeholder="Search" name="search">
               <button class="btn btn-outline-success my-2 my-sm-0"  type="submit">Search</button>
             </form>
             <a href="login.php" class="btn btn-outline-warning btn-md mr-3" role="button">LOGIN</a>
             <a href="register.php" class="btn btn-outline-info btn-md" role="button" >REGISTER</a>
           </ul>    
       </div>
      </nav> 

      <!-- JUMBOTRON -->
      <div class="jumbotron jumbotron-fluid bg-success text-light">
        <div class="container">
          <hr class="mt-4 bg-light">
          <h1 class="display-3 text-center">"Let's Go Invent Tomorrow Instead of Worrying about What Happended Yesterday"</h1>
          <blockquote class="blockquote text-right text-light h1">—Steve Jobs</blockquote>
          <hr class="mt-4 bg-light">
        </div>
      </div>

      <!-- NEWSFEED -->
      <?php foreach($blog as $post): ?>
      <div class="container">
        <a href="readmore.php?id=<?php echo $post->id ?>"><h1 class="my-4"><?= $post->title ?></h1></a>
        <p class="text-justify lead mb-4"><?= $post->body ?></p>
        <a href="readmore.php?id=<?php echo $post->id ?>" class="btn btn-outline-success btn-md" role="button">ReadMore</a>
        <hr class="bg-light">        
      </div>
      <?php endforeach; ?>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>
