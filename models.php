<?php
    /* DB CONNECTION */
    require 'db.php';
    
    function getAllBlogRow(){
        $connection = getConnection();
        $sql = 'SELECT * FROM blog'; 
        $statement = $connection->prepare($sql);
        $statement->execute();
        $blog = $statement->fetchAll(PDO::FETCH_OBJ);
        return $blog;
    }

    function getCertainBlogRow(){
        $connection = getConnection();
        $id = $_GET['id'];
        $sql = 'SELECT * FROM blog WHERE id=:id';
        $statement = $connection->prepare($sql);
        $statement->execute([':id' => $id]);
        $blog = $statement->fetch(PDO::FETCH_OBJ);
        return $blog;
    }

    function insertPost(){
        $connection = getConnection();     
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $title = $_POST['title'];
            $body = $_POST['body'];
            $fullbody = $_POST['fullbody'];
            $sql = 'INSERT INTO blog(title,body,fullbody) VALUES(:title, :body, :fullbody)';
            $statement = $connection->prepare($sql);
            if($statement->execute([':title' => $title, ':body' => $body, ':fullbody' => $fullbody])){
             return $message = 'message';
           }}}

    function deletePost(){
        $connection = getConnection();
        $id = $_GET['id'];
        $sql = 'DELETE FROM blog WHERE id=:id';
        $statement = $connection->prepare($sql);
        if($statement->execute([':id' => $id])){
            header("Location: index.php");
    }}

    function updatePost(){
        $connection = getConnection();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $id = $_GET['id'];
            $title = $_POST['title'];
            $body = $_POST['body'];
            $sql = 'UPDATE blog SET title=:title, body=:body WHERE id=:id';
            $statement = $connection->prepare($sql);
            if($statement->execute([':title' => $title, ':body' => $body, ':id' => $id])){
                return $message = 'Updated Succesfully';
    }}}

    function searchPost(){
        $connection = getConnection();
            $search = $_GET['search'];
            $sql = 'SELECT * FROM blog WHERE title LIKE :title';
            $connection = getConnection();
            $statement = $connection->prepare($sql);
            //$statement->bindValue(':title','%'.$search.'%');
            $statement->execute([':title' => '%'.$search.'%']);
            return $blog = $statement->fetchAll(PDO::FETCH_OBJ);
          }   

    function loginIntoAccount(){
        $connection = getConnection();
        if(empty($_POST['username']) || empty($_POST['password'])){
            $mes = "Please Fill All Field";
          }else{
            $sql = "SELECT * FROM users WHERE username =:username AND password =:password";
            $statement = $connection->prepare($sql);
            $statement->execute([':username' => $_POST['username'], ':password' => $_POST['password']]);
            $count = $statement->rowCount();
            if($count > 0){
             $_SESSION['username'] = $_POST['username'];
              header("Location:admin.php");
            }else{
              $mes = "Username / password not valid";
          }}
    }
?>  

    