<?php 
require 'models.php';

 session_start();
 $connection = getConnection();

  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(empty($_POST['username']) || empty($_POST['password'])){
      $mes = "  Please Fill All Field  ";
    }else{
      $password = sha1($_POST['password']);
      $sql = "SELECT * FROM users WHERE username =:username AND password =:password";
      $statement = $connection->prepare($sql);
      $statement->execute([':username' => $_POST['username'], ':password' => $password ]);
      $count = $statement->rowCount();
      if($count > 0){
       $_SESSION['username'] = $_POST['username'];
        header("Location:admin.php");
      }else{
        $mes = "Username / password not valid";
    }}}
         
    


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TECHSPOT | HOME</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>

      <!-- NAVBAR -->
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark sticky-top">
       <div class="container">
           <a class="navbar-brand h4" href="index.php">TECH<span class="text-success">SPOT</span> </a>
           <ul class="navbar-nav">
             <li class="nav-item active"><a class="nav-link" href="index.php"> <b> Home</b></a></li>
             <form class="form-inline my-2 my-lg-0 mr-3">
               <input class="form-control mr-sm-2" placeholder="Search" name="search">
               <button class="btn btn-outline-success my-2 my-sm-0"  type="submit">Search</button>
             </form>
             <a href="login.php" class="btn btn-outline-warning btn-md" role="button">LOGIN</a>
           </ul>    
       </div>
      </nav> 

      <!-- JUMBOTRON -->
      <div class="jumbotron jumbotron-fluid bg-success text-light">
        <div class="container">
          <hr class="mt-4 bg-light">
          <h1 class="display-3 text-center">USER LOGIN</h1>
          <hr class="mt-4 bg-light">
        </div>
      </div>


    <?php if(isset($mes)): ?>
      <div class="alert alert-danger container" role="alert">
          <?php echo $mes; ?>
      </div>
    <?php endif; ?>


   <form method='post'>
     <div class="container">
     <div class="form-group ">
        <label for="username h4" class="h4">Username</label>
        <input name="username" type="text" placeholder="Username" class="form-control form-control-lg">
     </div>
     <div class="form-group">
        <label for="password" class="h4">Password</label>
        <input name="password" type="password" placeholder="Password" class="form-control form-control-lg">
     </div>
     <button name="login" class="btn btn-danger mb-5 btn-block btn-lg" type="submit"><b>LOGIN</b></button>
     </div>
   </form>
    

     

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>
