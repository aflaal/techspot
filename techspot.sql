-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2018 at 08:47 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `techspot`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `fullbody` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `body`, `fullbody`) VALUES
(23, '  How to Become a Web Developer v2017', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Quisque eu ullamcorper elit, non tristique enim. Duis vel orci gravida, tincidunt leo nec, sollicitudin ante. Morbi sit amet euismod arcu, quis imperdiet nunc. Ut interdum, quam eu imperdiet imperdiet, mauris tellus facilisis metus, vel commodo nunc lectus maximus lorem. Maecenas ante risus, fermentum vitae sapien vitae, semper mattis orci. Maecenas mattis semper tortor pharetra lacinia. Nulla eu tincidunt ante.\r\n\r\nMorbi volutpat rhoncus accumsan. Suspendisse neque magna, vehicula suscipit pellentesque eget, vestibulum nec ipsum. Curabitur ultrices erat nec orci pulvinar, et gravida est ornare. Etiam posuere ut ipsum at volutpat. Curabitur aliquet, ante sit amet volutpat malesuada, enim metus suscipit dolor, sit amet sodales lacus mi ac justo. Curabitur cursus volutpat ipsum a porttitor. Quisque condimentum tortor ut tempor finibus. Phasellus efficitur elementum consequat.\r\n\r\nSed laoreet neque sit amet massa lacinia, vel aliquam arcu pretium. Aliquam erat volutpat. Donec volutpat, enim eget bibendum hendrerit, lacus nibh placerat leo, sit amet rutrum mi urna eget magna. Aliquam erat volutpat. Phasellus bibendum, metus a pulvinar pretium, dolor risus ornare urna, quis tincidunt dui diam ut leo. Proin ultricies laoreet ex, sed molestie nisi hendrerit commodo. Praesent tincidunt ex tellus, id varius purus feugiat id. Ut malesuada nibh vitae turpis pulvinar posuere. Quisque facilisis bibendum lacus interdum semper. Nullam et ante aliquet, aliquam nisl vel, luctus nisi. Fusce nec lorem neque. Ut bibendum tristique odio ut pulvinar. Donec elementum, lorem nec faucibus bibendum, ipsum erat venenatis tortor, ac facilisis magna metus vitae eros.'),
(24, 'Bootstrap 4 New Features', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Quisque eu ullamcorper elit, non tristique enim. Duis vel orci gravida, tincidunt leo nec, sollicitudin ante. Morbi sit amet euismod arcu, quis imperdiet nunc. Ut interdum, quam eu imperdiet imperdiet, mauris tellus facilisis metus, vel commodo nunc lectus maximus lorem. Maecenas ante risus, fermentum vitae sapien vitae, semper mattis orci. Maecenas mattis semper tortor pharetra lacinia. Nulla eu tincidunt ante. Morbi volutpat rhoncus accumsan. Suspendisse neque magna, vehicula suscipit pellentesque eget, vestibulum nec ipsum. Curabitur ultrices erat nec orci pulvinar, et gravida est ornare. Etiam posuere ut ipsum at volutpat. Curabitur aliquet, ante sit amet volutpat malesuada, enim metus suscipit dolor, sit amet sodales lacus mi ac justo. Curabitur cursus volutpat ipsum a porttitor. Quisque condimentum tortor ut tempor finibus. Phasellus efficitur elementum consequat. Sed laoreet neque sit amet massa lacinia, vel aliquam arcu pretium. Aliquam erat volutpat. Donec volutpat, enim eget bibendum hendrerit, lacus nibh placerat leo, sit amet rutrum mi urna eget magna. Aliquam erat volutpat. Phasellus bibendum, metus a pulvinar pretium, dolor risus ornare urna, quis tincidunt dui diam ut leo. Proin ultricies laoreet ex, sed molestie nisi hendrerit commodo. Praesent tincidunt ex tellus, id varius purus feugiat id. Ut malesuada nibh vitae turpis pulvinar posuere. Quisque facilisis bibendum lacus interdum semper. Nullam et ante aliquet, aliquam nisl vel, luctus nisi. Fusce nec lorem neque. Ut bibendum tristique odio ut pulvinar. Donec elementum, lorem nec faucibus bibendum, ipsum erat venenatis tortor, ac facilisis magna metus vitae eros.'),
(25, 'Ethical Hacking', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Quisque eu ullamcorper elit, non tristique enim. Duis vel orci gravida, tincidunt leo nec, sollicitudin ante. Morbi sit amet euismod arcu, quis imperdiet nunc. Ut interdum, quam eu imperdiet imperdiet, mauris tellus facilisis metus, vel commodo nunc lectus maximus lorem. Maecenas ante risus, fermentum vitae sapien vitae, semper mattis orci. Maecenas mattis semper tortor pharetra lacinia. Nulla eu tincidunt ante. Morbi volutpat rhoncus accumsan. Suspendisse neque magna, vehicula suscipit pellentesque eget, vestibulum nec ipsum. Curabitur ultrices erat nec orci pulvinar, et gravida est ornare. Etiam posuere ut ipsum at volutpat. Curabitur aliquet, ante sit amet volutpat malesuada, enim metus suscipit dolor, sit amet sodales lacus mi ac justo. Curabitur cursus volutpat ipsum a porttitor. Quisque condimentum tortor ut tempor finibus. Phasellus efficitur elementum consequat. Sed laoreet neque sit amet massa lacinia, vel aliquam arcu pretium. Aliquam erat volutpat. Donec volutpat, enim eget bibendum hendrerit, lacus nibh placerat leo, sit amet rutrum mi urna eget magna. Aliquam erat volutpat. Phasellus bibendum, metus a pulvinar pretium, dolor risus ornare urna, quis tincidunt dui diam ut leo. Proin ultricies laoreet ex, sed molestie nisi hendrerit commodo. Praesent tincidunt ex tellus, id varius purus feugiat id. Ut malesuada nibh vitae turpis pulvinar posuere. Quisque facilisis bibendum lacus interdum semper. Nullam et ante aliquet, aliquam nisl vel, luctus nisi. Fusce nec lorem neque. Ut bibendum tristique odio ut pulvinar. Donec elementum, lorem nec faucibus bibendum, ipsum erat venenatis tortor, ac facilisis magna metus vitae eros.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin'),
(2, 'afl', 'afl'),
(8, 'aflaal', 'afl');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
